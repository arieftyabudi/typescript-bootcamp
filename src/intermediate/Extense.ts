// extense in typescript is a contex a part of something

//In this section it tells,is 2(number) is part of typeof number and number1 is true
type number1 = 2 extends number ? true : false;

// in this section number2 = false cause number can be anything than typeof number it self it can be 10 100 1000 or nything that typeof number
type number2 = number extends 10 ? true : false;

//in this section array1= false casuse number[] it mean specific array of number,but [] it can be anything that comes an array it can be string[] or symbol[] or anything
type array1 = number[] extends [] ? true : false;

//In this section array2 = true cause [] it extends from number[],it mean is array part of number of array,the answer is yes cause number[] is an array to
type array2 = [] extends number[] ? true : false;

//In this section array3 = true casuse it part of any[],any[] including number[],string[],symbol[] or anything that comes as long as is array type
type array3 = number[] extends any[] ? true : false;
