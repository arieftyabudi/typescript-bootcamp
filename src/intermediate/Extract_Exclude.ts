//Extract & Exclude is build in method in TS

type Fruits =
  | "Apple"
  | "Strawberry"
  | "Banana"
  | { fruits: true; vegetables: false };

// StringFruits="Apple" |"Strawbery"|"Banana"
type StringFruits = Extract<Fruits, string>;

// now ObjectFruits will contain only object {fruits:true,vegetables:false}
type ObjectFruits = Extract<Fruits, object>;

// now nonStringFruits will contain only object item {fruits:true,vegetables:false}
type nonStringFruits = Exclude<Fruits, string>;

// now nonObjectFruits will contain only string type only "Apple" |"Strawbery"|"Banana"
type nonObjectFruits = Exclude<Fruits, object>;
