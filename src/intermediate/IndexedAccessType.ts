//Indexed access type

type Animal = {
  name: string;
  legs: number;
  lungs: boolean;
  ears: boolean;
  skills: {
    run: boolean;
    swim: boolean;
    fly: boolean;
  };
};

//it will contain only run,swim,fly
type animalSkills = Animal["skills"];

//It will contain number and {run:boolean,swin:boolean,fly:boolean}
type animalSkills2 = Animal["skills" | "legs"];
