class Admin {
  create() {}
  update() {}
  delete() {}
  read() {}
}

class Guest {
  read() {}
  comment() {}
}

type Orang<Type> = Type extends "admin" ? Admin : Guest;

const person: Orang<"admin"> = new Admin();

const person2: Orang<"guest"> = new Guest();
