class Person {
  constructor(public name: string, public age: number) {}

  greet(): string {
    return `hello my name is ${this.name} and I am ${this.age} years old`;
  }
}

class Student extends Person {
  constructor(
    public name: string,
    public age: number,
    public id: string | number
  ) {
    super(name, age);
  }

  study(): string {
    return `Student ${this.name} (${this.id}) is studying`;
  }
}

let student = new Student("Arief Setyabudi", 100, "EU2075");

let greet = student.greet();

let study = student.study();

