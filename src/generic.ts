function getData(value: any) {
  return value;
}

console.log(getData("Setyabudi").length);
//it will return undefine cause number dont have method length
console.log(getData(1234).length);

// using generic

function myData<T>(value: T) {
  return value;
}

console.log(myData("Setyabudi").length);
console.log(myData(123));

// using arrow function

const arrowFunc = <T>(value: T) => {
  return value;
};

const responseApiBook = {
  success: true,
  message: "Succes get data",
  data: [
    { id: 1, title: "Bobo", page: 200 },
    {
      id: 2,
      title: "koran lampu merah",
      page: 40,
    },
  ],
  lala: "asdf",
};

const responseApiUser = {
  success: true,
  message: "Succes get data",
  data: [{ id: 1, fullName: "Ranger merah", role: "Admin" }],
};

type ResponseApi<Type> = {
  success: boolean;
  message: string;
  data: Type;
};

type Book = {
  id: number;
  title: string;
  page: Number;
};

type User = {
  id: number;
  fullName: string;
  role: string;
};

const listBook: ResponseApi<Book[]> = responseApiBook;
console.log(listBook);
