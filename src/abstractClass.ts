/* abstract class a class that cannot immediately to instance from that outside that class,must extense that child class so 
abstract class it doesn iherit the property in parent but when extended we need assign property of new extended that class*/

abstract class Vehicle {
  abstract wheels: number ;

  start(): void {
    console.log("brummmm");
  }
}

class Car extends Vehicle{
wheels: number = 4;
}

class MotoCycle extends Vehicle{
    wheels = 2
}

let car = new Car()
console.log(car.wheels)
car.start()

let motor = new MotoCycle()
console.log(motor.wheels)
car.start()