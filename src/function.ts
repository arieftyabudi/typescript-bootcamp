//function that return string value
function getName(): string {
  return "hello,my name is Arief";
}

console.log(getName());

//in arrow function
let getNameMe = (): string => {
  return "my name is Arief";
};
console.log(getNameMe());

//function that return number value
function getAge(): number {
  return 100;
}
console.log(getAge());

let getAgeMe = (): number => {
  return 100;
};

console.log(getAgeMe());

//if function not return anything

function printName(): void {
  console.log("Hello my name is Arief");
}

printName();

const printNameMe = (): void => {
  console.log("Hello my name is Arief");
};

printNameMe();

/*-------------------------------------------------------------------------------------------------------------------------------------*/

//parameter typescript in function

function extraction(val1: number, val2: number): number {
  return val1 + val2;
}
let result = extraction(7, 7);
console.log(result);

/*---------------------------------------------------------------------------------------------------------------------------------------*/

// Function as type

type Extraction = (val1: number, val2: number) => number;

let tambah:Extraction =(angka1,angka2)=>{
    return angka1+angka2
}

console.log(tambah(8,8))

/*----------------------------------------------------------------------------------------------------------------------------------------*/

//default parameter function

function fullName(firstName:string,lastName:string ="nama belakang"):string{
return `${firstName} ${lastName}`
}
console.log(fullName("nama depan"))

/*---------------------------------------------------------------------------------------------------------------------------------------*/
//optional parameter in fuction

//the parameter will get optional not error but return undefine and if it's type assigment is number it's not advisable using optional type
function getString(val1:string ,val2?:string):string{
  return val1 + " " + val2
}

console.log(getString('Coding is','fun'))

