export class User {
  public name: string;

  //public property can be assign in custructor using public flags and does'n require to reassign using this

  constructor(name: string, public age: number) {
    this.name = name;
  }

  setName(value: string): void {
    this.name = value;
  }

  getName = (): string => {
    return this.name;
  };
}

let user = new User("Setyabudi", 100);
console.log(user);

/*-----------------------------------------------------------------------------------------*/

//inheritance in class oop

//public = can be accessed in all class / outside its's own class
//protected = can be accessed from that class, and it's all class inheritance
//private = can be accessed from that own class

class Admin extends User {
  read: boolean = true;
  write: boolean = true;
  phone: string;
  private _email: string = "";
  //static method can be accessed withis that class without new instance of that class eg. let admin = new Admin()
  static getRoleName: string = "Admin";

  //static method
  static greetingMessage(): string {
    return "hello admin";
  }

  constructor(phone: string, name: string, age: number) {
    super(name, age);
    this.phone = phone;
  }

  getRole(): { read: boolean; write: boolean } {
    return {
      read: this.read,
      write: this.write,
    };
  }
  /*this name just to explain if setter is called /name setter/=value cause 
class already know when property in this class have assigned = class already know it's a setter*/

  // set setEmail(value:string){
  //     if(value.length < 5){
  //         this._email ="Wrong email format"
  //     }else{
  //         this._email = value
  //     }
  // }

  //real use case

  set email(value: string) {
    if (value.length < 5) {
      this._email = "Wrong email format";
    } else {
      this._email = value;
    }
  }

  get email(): string {
    return this._email;
  }
}
/*if u called specific class when initiate new instance of that class the parameter 
using that constructor params*/

let admin = new Admin("0812200003", "tono", 25);
console.log(admin);
console.log(admin.getName());
console.log(admin.getRole());

admin.setName("dadang carbin");
console.log(admin.getName());

/*to explain setter have to diffrentsiate the name of is setter in class
admin.setEmail ="test@test.com"*/

admin.email = "test@test.com";
console.log(admin.email);

//private type
//console.log(admin._email) produce error cause can be accessed in admin class only cannot accessed outside it's class

//static property and methods
let staticAdminProperty = Admin.getRoleName;
let staticAdminMethods = Admin.greetingMessage();
console.log(staticAdminMethods);
console.log(staticAdminProperty);
