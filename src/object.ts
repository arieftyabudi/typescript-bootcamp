//Object in ts

type BioData = {
  name: string;
  age: number;
};

let user: BioData = {
  name: "Arief",
  age: 100,
};

// this will trows a error cause in in type Biodata we define two property
// let user: BioData = {
//     name: "Arief",
//     age: 100,
//     race:"super saiyan"
//   };
  
