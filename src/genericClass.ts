class List<Type> {
  private data: Type[];

  constructor(...elements: Type[]) {
    this.data = elements;
  }

  add(elements: Type): void {
    this.data.push(elements);
  }

  addMultiple(...element: Type[]) {
    this.data.push(...element);
  }

  getData(): Type[] {
    return this.data;
  }
}

// let numbers = new List<number>(1, 2, 3);
// numbers.add(5);
// numbers.addMultiple(4,5,6,7)
// console.log(numbers.getData());

let random = new List<number | string | object>(1, "asd", 3);
random.addMultiple("asd", 234, "dfgd", { id: 1, name: "android" });
console.log(random.getData());
