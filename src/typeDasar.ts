// string variable
let nama: string = "lalal";

nama = "must be a string";

//number variable

let number : number 
number=50
number=80

//boolean variable

let isAdmin : boolean

isAdmin =true
isAdmin=false

// type any *litterally ultimate skill if your project near death line 

let heroes :any

heroes="Iron mamen"
heroes=["irin mamen","dadang sekrup"]
heroes=90
heroes ={
    lead:"iron mamen",
    skill:"laser beam"
}
heroes=true || false || "string"

//array variables

//asigment to variable array
let barudak :string[] 

//or this using angle bracket
let barudak1 : Array<number>

//using ultimate any
let anything : any[]

barudak=['mamng japrut','dadang sekop','aceng jerman']

barudak1=[1,2,3]

anything=["test",1,true,{}]

// array tuples 
//array that contains any type data but the value it's limited to it's contex

let tuples :[string,number]

tuples=["dadang sekrup",90]

/*
it will prodece error because we assigment 2 type but in variable assigment 3 value 

/*ERROR/
tuples=['dadang jahe',89,true]
*/

//union type data

let phone : number|string

phone="0812200003"
phone=62123456

