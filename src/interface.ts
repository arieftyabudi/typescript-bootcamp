interface Laptop {
  name: string;
  on(): void;
  off(): void;
}

class Asus implements Laptop {
  constructor(public name: string, public isGaming: boolean) {}

  on(): void {
    console.log("Menyala");
  }

  off(): void {
    console.log("Mati");
  }
}

class MacBook implements Laptop {
  constructor(public name: string, public keyboardLigth: boolean) {}

  on(): void {
    console.log("Menyala");
  }

  off(): void {
    console.log("Mati");
  }
}

let gamingLaptop = new Asus("Agx001", true);
console.log(gamingLaptop);
console.log(gamingLaptop.on());
console.log(gamingLaptop.off());

let mac = new MacBook("Apple001", true);
console.log(mac);
console.log(mac.on());
console.log(mac.off());
