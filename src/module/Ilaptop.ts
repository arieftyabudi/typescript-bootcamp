// interface is a blueprint was build for rules that must be implemented in laptop class also in inherit them
export default interface Ilaptop<Type>{
    name:string
    type:Type
    withNumeric:boolean
    withTouchButton:boolean
    withVga:boolean
    iCloudAccess:boolean
}