import Asus from "./Asus";
import MacBook from "./MacBook";

let asus = new Asus<string | number>("Zenbook", true, true, true, false);
console.log(asus);

let macBook = new MacBook<number>(12345, true, false, false, true);
console.log(macBook);

asus.on();
asus.a();
asus.b();
asus.off();

macBook.on();
macBook.a();
macBook.b();
macBook.off();
