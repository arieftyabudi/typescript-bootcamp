import Ilaptop from "./Ilaptop";
//Using wildcard to import all and name spaced as Keyboard
import * as Keyboard from "./Keyboard";
//using destructuring when assigment just using original name of imported method
import { keyB, off } from "./Keyboard";

//in this section i use abstract cause baseLaptop cannot be direct accessed without inheritance so it's build for class inheritance  
export default abstract class BaseLaptop<Type> implements Ilaptop<Type> {
  constructor(
    public name: string,
    public type: Type,
    public withNumeric: boolean,
    public withTouchButton: boolean,
    public withVga: boolean,
    public iCloudAccess: boolean
  ) {}
  //Using wildcard to import all and name spaced as Keyboard
  a() {
    Keyboard.keyA();
  }
  //using destructuring when assigment just using original name of imported method
  b() {
    keyB();
  }
  on() {
    Keyboard.on();
  }
  off() {
    off();
  }
}
