import BaseLaptop from "./BaseLaptop";
//extends mean class Asus included in class Base Laptop
export default class Asus<Type> extends BaseLaptop<Type> {
  constructor(
    type: Type,
    withNumeric: boolean,
    withTouchButton: boolean,
    withVga: boolean,
    iCloudAccess: boolean
  ) {
    super("Asus", type, withNumeric, withTouchButton, withVga, iCloudAccess);
  }
}
