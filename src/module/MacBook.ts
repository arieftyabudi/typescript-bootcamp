import BaseLaptop from "./BaseLaptop";

export default class MacBook<Type> extends BaseLaptop<Type> {
  constructor(
    type: Type,
    withNumeric: boolean,
    withTouchButton: boolean,
    withVga: boolean,
    iCloudAccess: boolean
  ) {
    super("MacBook", type, withNumeric, withTouchButton, withVga, iCloudAccess);
  }
}
