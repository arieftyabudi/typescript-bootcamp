// constant value in type script

// if not provided assigment value to enums var it auto matic add number value that auto increment like array index

//number enums
enum Mounth {
  JAN = 1,
  FEB = 2,
  MAR = 3,
  APR,
  MAY,
}

console.log(Mounth);

//enum string

/*if enums string one of them not assinged value it will throw a error because the default value enums 
is incerement number so it will contain both number and string value*/

enum Rangers {
  red = "Raptor",
  blue = "Shark",
  green = "Triceratops",
  yellow = "Petrodactile",
  pink = "Archaeopteryx",
}

console.log(Rangers)