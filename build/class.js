"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
var User = /** @class */ (function () {
    //public property can be assign in custructor using public flags and does'n require to reassign using this
    function User(name, age) {
        var _this = this;
        this.age = age;
        this.getName = function () {
            return _this.name;
        };
        this.name = name;
    }
    User.prototype.setName = function (value) {
        this.name = value;
    };
    return User;
}());
exports.User = User;
var user = new User("Setyabudi", 100);
console.log(user);
/*-----------------------------------------------------------------------------------------*/
//inheritance in class oop
//public = can be accessed in all class / outside its's own class
//protected = can be accessed from that class, and it's all class inheritance
//private = can be accessed from that own class
var Admin = /** @class */ (function (_super) {
    __extends(Admin, _super);
    function Admin(phone, name, age) {
        var _this = _super.call(this, name, age) || this;
        _this.read = true;
        _this.write = true;
        _this._email = "";
        _this.phone = phone;
        return _this;
    }
    //static method
    Admin.greetingMessage = function () {
        return "hello admin";
    };
    Admin.prototype.getRole = function () {
        return {
            read: this.read,
            write: this.write,
        };
    };
    Object.defineProperty(Admin.prototype, "email", {
        get: function () {
            return this._email;
        },
        /*this name just to explain if setter is called /name setter/=value cause
      class already know when property in this class have assigned = class already know it's a setter*/
        // set setEmail(value:string){
        //     if(value.length < 5){
        //         this._email ="Wrong email format"
        //     }else{
        //         this._email = value
        //     }
        // }
        //real use case
        set: function (value) {
            if (value.length < 5) {
                this._email = "Wrong email format";
            }
            else {
                this._email = value;
            }
        },
        enumerable: false,
        configurable: true
    });
    //static method can be accessed withis that class without new instance of that class eg. let admin = new Admin()
    Admin.getRoleName = "Admin";
    return Admin;
}(User));
/*if u called specific class when initiate new instance of that class the parameter
using that constructor params*/
var admin = new Admin("0812200003", "tono", 25);
console.log(admin);
console.log(admin.getName());
console.log(admin.getRole());
admin.setName("dadang carbin");
console.log(admin.getName());
/*to explain setter have to diffrentsiate the name of is setter in class
admin.setEmail ="test@test.com"*/
admin.email = "test@test.com";
console.log(admin.email);
//private type
//console.log(admin._email) produce error cause can be accessed in admin class only cannot accessed outside it's class
var staticAdminProperty = Admin.getRoleName;
var staticAdminMethods = Admin.greetingMessage();
console.log(staticAdminMethods);
console.log(staticAdminProperty);
