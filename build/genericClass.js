var List = /** @class */ (function () {
    function List() {
        var elements = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            elements[_i] = arguments[_i];
        }
        this.data = elements;
    }
    List.prototype.add = function (elements) {
        this.data.push(elements);
    };
    List.prototype.addMultiple = function () {
        var _a;
        var element = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            element[_i] = arguments[_i];
        }
        (_a = this.data).push.apply(_a, element);
    };
    List.prototype.getData = function () {
        return this.data;
    };
    return List;
}());
// let numbers = new List<number>(1, 2, 3);
// numbers.add(5);
// numbers.addMultiple(4,5,6,7)
// console.log(numbers.getData());
var random = new List(1, "asd", 3);
random.addMultiple("asd", 234, "dfgd", { id: 1, name: "android" });
console.log(random.getData());
