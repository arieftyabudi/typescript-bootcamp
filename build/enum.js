// constant value in type script
// if not provided assigment value to enums var it auto matic add number value that auto increment like array index
//number enums
var Mounth;
(function (Mounth) {
    Mounth[Mounth["JAN"] = 1] = "JAN";
    Mounth[Mounth["FEB"] = 2] = "FEB";
    Mounth[Mounth["MAR"] = 3] = "MAR";
    Mounth[Mounth["APR"] = 4] = "APR";
    Mounth[Mounth["MAY"] = 5] = "MAY";
})(Mounth || (Mounth = {}));
console.log(Mounth);
//enum string
var Rangers;
(function (Rangers) {
    Rangers["red"] = "Raptor";
    Rangers["blue"] = "Shark";
    Rangers["green"] = "Triceratops";
    Rangers["yellow"] = "Petrodactile";
    Rangers["pink"] = "Archaeopteryx";
})(Rangers || (Rangers = {}));
console.log(Rangers);
