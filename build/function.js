//function that return string value
function getName() {
    return "hello,my name is Arief";
}
console.log(getName());
//in arrow function
var getNameMe = function () {
    return "my name is Arief";
};
console.log(getNameMe());
//function that return number value
function getAge() {
    return 100;
}
console.log(getAge());
var getAgeMe = function () {
    return 100;
};
console.log(getAgeMe());
//if function not return anything
function printName() {
    console.log("Hello my name is Arief");
}
printName();
var printNameMe = function () {
    console.log("Hello my name is Arief");
};
printNameMe();
/*-------------------------------------------------------------------------------------------------------------------------------------*/
//parameter typescript in function
function extraction(val1, val2) {
    return val1 + val2;
}
var result = extraction(7, 7);
console.log(result);
var tambah = function (angka1, angka2) {
    return angka1 + angka2;
};
console.log(tambah(8, 8));
/*----------------------------------------------------------------------------------------------------------------------------------------*/
function fullName(firstName, lastName) {
    if (lastName === void 0) { lastName = "nama belakang"; }
    return "".concat(firstName, " ").concat(lastName);
}
console.log(fullName("nama depan"));
