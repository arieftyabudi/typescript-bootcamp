"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//Using wildcard to import all and name spaced as Keyboard
var Keyboard = require("./Keyboard");
//using destructuring when assigment just using original name of imported method
var Keyboard_1 = require("./Keyboard");
var BaseLaptop = /** @class */ (function () {
    function BaseLaptop(name, type, withNumeric, withTouchButton, withVga, iCloudAccess) {
        this.name = name;
        this.type = type;
        this.withNumeric = withNumeric;
        this.withTouchButton = withTouchButton;
        this.withVga = withVga;
        this.iCloudAccess = iCloudAccess;
    }
    //Using wildcard to import all and name spaced as Keyboard
    BaseLaptop.prototype.a = function () {
        Keyboard.keyA();
    };
    //using destructuring when assigment just using original name of imported method
    BaseLaptop.prototype.b = function () {
        (0, Keyboard_1.keyB)();
    };
    BaseLaptop.prototype.on = function () {
        Keyboard.on();
    };
    BaseLaptop.prototype.off = function () {
        (0, Keyboard_1.off)();
    };
    return BaseLaptop;
}());
exports.default = BaseLaptop;
