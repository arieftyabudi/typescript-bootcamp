function getData(value) {
    return value;
}
console.log(getData("Setyabudi").length);
//it will return undefine cause number dont have method length
console.log(getData(1234).length);
// using generic
function myData(value) {
    return value;
}
console.log(myData("Setyabudi").length);
console.log(myData(123));
// using arrow function
var arrowFunc = function (value) {
    return value;
};
var responseApiBook = {
    success: true,
    message: "Succes get data",
    data: [
        { id: 1, title: "Bobo", page: 200 },
        {
            id: 2,
            title: "koran lampu merah",
            page: 40,
        },
    ],
    lala: "asdf",
};
var responseApiUser = {
    success: true,
    message: "Succes get data",
    data: [{ id: 1, fullName: "Ranger merah", role: "Admin" }],
};
var listBook = responseApiBook;
console.log(listBook);
