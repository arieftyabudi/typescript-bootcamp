var Asus = /** @class */ (function () {
    function Asus(name, isGaming) {
        this.name = name;
        this.isGaming = isGaming;
    }
    Asus.prototype.on = function () {
        console.log("Menyala");
    };
    Asus.prototype.off = function () {
        console.log("Mati");
    };
    return Asus;
}());
var MacBook = /** @class */ (function () {
    function MacBook(name, keyboardLigth) {
        this.name = name;
        this.keyboardLigth = keyboardLigth;
    }
    MacBook.prototype.on = function () {
        console.log("Menyala");
    };
    MacBook.prototype.off = function () {
        console.log("Mati");
    };
    return MacBook;
}());
var gamingLaptop = new Asus("Agx001", true);
console.log(gamingLaptop.on());
console.log(gamingLaptop.off());
var mac = new MacBook("Apple001", true);
console.log(mac);
console.log(mac.on());
console.log(mac.off());
