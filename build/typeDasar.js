// string variable
var nama = "lalal";
nama = "must be a string";
//number variable
var number;
number = 50;
number = 80;
//boolean variable
var isAdmin;
isAdmin = true;
isAdmin = false;
// type any *litterally ultimate skill if your project near death line 
var heroes;
heroes = "Iron mamen";
heroes = ["irin mamen", "dadang sekrup"];
heroes = 90;
heroes = {
    lead: "iron mamen",
    skill: "laser beam"
};
heroes = true || false || "string";
//array variables
//asigment to variable array
var barudak;
//or this using angle bracket
var barudak1;
//using ultimate any
var anything;
barudak = ['mamng japrut', 'dadang sekop', 'aceng jerman'];
barudak1 = [1, 2, 3];
anything = ["test", 1, true, {}];
// array tuples 
//array that contains any type data but the value it's limited to it's contex
var tuples;
tuples = ["dadang sekrup", 90];
/*
it will prodece error because we assigment 2 type but in variable assigment 3 value

/*ERROR/
tuples=['dadang jahe',89,true]
*/
